package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity {

    private FirebaseAuth myAuth;
    private EditText editTextLogin;
    private EditText editTextSenha;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        myAuth = FirebaseAuth.getInstance();
        editTextLogin = findViewById(R.id.editTextEmail);
        editTextSenha = findViewById(R.id.editTextSenha);

    }

    public void login(View view){
        myAuth.signOut();

        String login = editTextLogin.getText().toString();
        String senha = editTextSenha.getText().toString();

        if(!login.trim().equals("")) {
            myAuth.signInWithEmailAndPassword(login, senha)
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                Toast.makeText(getApplicationContext(), "Sucesso" + myAuth.getCurrentUser().getEmail(), Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(getApplicationContext(), Principal.class);
                                startActivity(intent);
                            } else {
                                Toast.makeText(getApplicationContext(), "Falha ", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
        }
    }

    public void cadastrar(View view){
        Intent intent = new Intent(getApplicationContext(), Cadastro.class);
        startActivity(intent);
    }

    public void recuperar(View view){
        if(!editTextLogin.getText().toString().trim().equals("")){
            myAuth.sendPasswordResetEmail(editTextLogin.getText().toString());
        }
    }

}
